%-----------------------------------------------------------------------------%
\chapter{\babDua}
%-----------------------------------------------------------------------------%

This chapter talks about the work during the internship period and an analysis
of the internship results.

%-----------------------------------------------------------------------------%
\section{Internship Work}
%-----------------------------------------------------------------------------%

This section talks about the work background, work description, literature
review, technologies used, work results, and non-technical aspects of the
internship.

%-----------------------------------------------------------------------------%
\subsection{Work Background and Description}
%-----------------------------------------------------------------------------%

\instansi\ regularly releases new products with its partners on a monthly
basis. Most of the products are web-based applications that are built with
Django and Angular. \instansi's team of developers often see similar patterns
or features in the applications they've built, such as user registration, email
verification, media uploads, etc. To speed up the development of new products,
they built a codebase generator tool with a project template which comprises
these features.

However, due to the increasing number of features and insufficient resources to
maintain the template, it has become riddled with bugs and issues. This hinders
the development of new projects, which impacts \instansi's performance as a
company. Thus, this internship initially aimed to solve that problem by fixing
the issues within the template and developing new features for the template to
make it easier to use for new developers.

In addition to working on the template, the internship also included tasks to
work on several \instansi's customer projects. The tasks were to debug and
develop features for both new and existing projects. This provided an
opportunity to better understand the development flow and practices at
\instansi.

In the first two weeks of the internship, the work was focused on the codebase
generator tool. The work started by identifying the issues that existed in the
template. This was done by trying to generate and develop a simple project
using the tool. Once the issues had been identified, the possible solutions
were discussed with the supervisor and/or other developers. The decided
solutions would be implemented and made as merge requests to the tool's
repository to be reviewed and merged. After working on the template, the
internship work was mostly done for \instansi's company website and three
projects for \instansi's customers.

The tasks for \instansi's website were mainly focused on adding a new Success
Stories page. The page would show \instansi's previous customers and the
products \instansi\ had made for them. The main reason for adding this new
page was to convince potential customers of what they can build with \instansi.
The tasks also involved improving the website by fixing accessibility issues
and implementing a better form submission method using AJAX.

The first customer project to work on was the Aiconix Slack Bot project.
Aiconix is a startup that specializes in providing AI technologies for media
files \cite{aiconix:site}. One of the services provided by Aiconix is an
automated video and audio transcription service for corporates. \instansi\ has
been developing a Slack bot for Aiconix that integrates this service within the
Slack environment. However, it had an issue with handling WebM files and media
files that have incomplete metadata. It also had another issue with downloading
files from Slack which sometimes resulted in file corruption. The task was to
fix the two issues and other minor issues, such as error handling and wrong
translations.

The second project was the Taleegator MVP project. Taleegator is a startup that
specializes in providing human resources development tools for businesses
\cite{taleegator:site}. \instansi\ had just begun developing a web-based
application for Taleegator and it hadn't been deployed anywhere. The task was
to set up the CI/CD system, which includes setting up automated tests and
deployment to \instansi's private server.

The third project, which took up most of the internship, was the Augmented
Science MVP project. Augmented Science is a startup that aims to provide
scientists an easy way to showcase their projects at science events by
utilizing Augmented Reality technologies. In this project, the work was mostly
done collaboratively with the other intern and another developer (who's also
the Project Manager) to build an Augmented Reality application from the start.
The primary task was to develop the backend service that connects the mobile
and web application with a third-party Augmented Reality service.

%-----------------------------------------------------------------------------%
\subsection{Literature Review}
%-----------------------------------------------------------------------------%

This subsection explains the underlying concepts of the work done during the
internship.

\subsubsection{Continuous Integration and Continuous Delivery/Deployment}

Continuous integration (CI) is a software development practice of integrating
the work of each team member frequently, usually multiple times a day. Each
integration is verified by automatically building and testing it to quickly
detect integration errors. This aims to reduce integration problems within
the development process and allows a team to rapidly develop cohesive software
\cite{martin:ci}. CI is often combined with either continuous delivery or
continuous deployment.

Continuous delivery (CDE) extends the concept of CI
by ensuring that the build is always ready for the production environment after
passing the automated tests and quality checks
\cite{DBLP:journals/corr/ShahinBZ17}. Continuous deployment (CD) takes this a
step further by automatically releasing every change that passes all stages
of the production pipeline \cite{atlassian:pittet_cicd}. Continuous deployment
implies continuous delivery, but not the other way around, as continuous
deployment requires no human intervention in the deployment process. This is
illustrated by \autoref{fig:cicd}, where it shows that in Continuous
Deployment, "Deploy to Production" is always an automatic task.

\begin{figure}
	\centering
	\includegraphics[width=1.00\textwidth]{pics/cicd.png}
	\captionsource{Continuous Delivery (CDE) vs Continuous Deployment
	(CD).}{\url{https://atlassian.com/continuous-delivery/principles/continuous-integration-vs-delivery-vs-deployment} (redrawn)}
	\label{fig:cicd}
\end{figure}

\subsubsection{Augmented Reality}

Augmented Reality (AR) is a technology that involves the overlay of computer
graphics on the real world using three main components: a scene generator,
a tracking system, and a display device. The scene generator renders the scene
by combining virtual objects on top of the real world. The tracking system
ensures that the virtual objects are properly aligned with the real world.
The rendered scene is shown on a display device, which can either be embedded
in a wearable device or it can utilize common display devices, such as phone
screens \cite{paper:intro_ar}.

\begin{figure}
	\centering
	\copyrightbox[b]{
		\includegraphics[width=0.50\textwidth]
		{pics/ar.jpg}
	}{Source: \url{https://commons.wikimedia.org/wiki/File:Augmented_Reality_for_eCommerce.jpg}}
	\caption{The use of Augmented Reality in an e-commerce application.}
	\label{fig:ar}
\end{figure}

\subsubsection{Representational State Transfer (REST)}

Representational state transfer (REST) is a software architectural style for
creating web services. Web services that conform to REST constraints, called
RESTful web services, give computer systems the ability to interoperate between
each other on the internet. RESTful web services allow the access and
manipulation of web resources in their textual representations by using a
predefined and uniform set of stateless operations.

The formal REST constraints are: client-server architecture, statelessness,
cacheability, uniform interface, layered system, and code-on-demand (optional).
The client-server architecture separates user interface concerns from data
storage concerns, which improves portability of the user interface and
simplifies the server components. The stateless constraint reduces load on the
server by not having to manage resource usage across requests. The cache
constraint improves network efficiency by reusing data for multiple requests.
The uniform interface constraint ensures that implementations are decoupled
from the services they provide. The layered system constraint promotes
encapsulation by ensuring communication only happens between one component and
another component in the immediate layer. Finally, the optional code-on-demand
constraint allows the client functionality to be extended by downloading and
executing code in the form of applets and scripts \cite{paper:rest}.

The uniform interface constraint is defined further by four other constraints.
The first constraint is the identification of resources in requests, such as
using URIs. The second constraint is the manipulation of resources through
representations, which lets the client modify or delete resources using the
given representations. The third constraint is self-descriptive messages, which
means that each message has enough information to describe how it should be
processed. The final constraint is hypermedia as the engine of application
state (HATEOAS), which lets the client to navigate through all the available
resources it needs without having to know the structure of the application
\cite{rest:hateoas}.

%-----------------------------------------------------------------------------%
\subsection{Workflow}
%-----------------------------------------------------------------------------%

The main workflow for each day during the internship was as follows:

\begin{enumerate}[wide, label=\textbf{\arabic*.}, labelwidth=!, labelindent=0pt]
	\item \bo{Daily meeting} \\
	Before starting each day, a meeting with the supervisor and/or project
	manager was held to discuss the things that need to be done for the
	day. The meeting might also discuss the tasks from the previous day.
	\item \bo{Tasks creation and assignment} \\
	The tasks that arose from the discussion were put into Productive, a
	project management tool used by \instansi. Each task was assigned to one
	person at a time. Tasks assignment was also agreed upon in the meeting.
	During this stage, the tasks were marked as "to do".
	\item \bo{Development} \\
	Each person would spend the day working on the tasks, which were mainly
	software development tasks. In addition to writing implementation code,
	this might also include writing tests and documentation. During this stage,
	the tasks were marked as "in progress".
	\item \bo{Work submission and peer reviews} \\
	Every time a software development task was finished, a merge request should
	be created with a link to the relevant task on Productive. This would
	automatically mark the task as "in review". Before a merge request could be
	merged, it should be reviewed by other members of the development team. The
	merge request author should update their work accordingly. An example of a
	code review is shown in \autoref{fig:codereview}, in which another
	developer discussed a different approach in fixing the problem.
	\item \bo{Tasks resolution} \\
	Once a merge request had passed peer reviews and tests, it would be merged
	into the master branch of the repository. This would automatically mark the
	task as "on master". If continuous deployment was enabled, then the changes
	would automatically be deployed to production.
\end{enumerate}

\begin{figure}
	\centering
	\includegraphics[width=0.85\textwidth]
	{pics/codereview.png}
	\caption{A code review example in the Aiconix project.}
	\label{fig:codereview}
\end{figure}

%-----------------------------------------------------------------------------%
\subsection{Technologies}
%-----------------------------------------------------------------------------%

The main programming languages used in the internship were Python and
JavaScript. Python was used for the backend parts of the projects, which were
built using the Django web framework. Most of \instansi's projects also utilize
the Django REST Framework to build RESTful APIs. JavaScript was used for the
frontend parts, which were mostly built with Angular.

For distributed task queues in the backend, \instansi\ mainly uses the Celery
package with a Redis broker. However, for simplicity, Django-Q was used instead
of Celery for the Augmented Science project. These tools can provide a way to
do long-running tasks asynchronously, without blocking the request-response
cycle in a web server.

\instansi\ uses Hugo to generate static sites, such as its company website. Hugo
is a static site generator written in the Go programming language. Hugo allows
its user to generate HTML pages with markdown files. Hugo renders the markdown
content inside user-defined templates, which can be composed of multiple
partial templates.

\instansi\ uses Git as its version control system (VCS). Git repositories of
customer projects are hosted on \instansi's own GitLab server. \instansi\ also
maintains open source projects (including forks) on GitLab.com and GitHub.

In its CI/CD configuration, \instansi\ uses coala, a static code analysis tool
built by \instansi's CEO, to lint code files. Deployments are configured with
Docker and Docker Compose. On the private server, the infrastructure uses
Docker Swarm to manage clusters of Docker Engines. It also uses Traefik as the
router.

In the non-technical aspect, \instansi\ uses a self-hosted Mattermost, an
open-source alternative to Slack, as the primary channel of communication.
\instansi\ uses Discord for voice communication and Zoom for video meetings.
To manage tasks and employees' work hours, \instansi\ uses Productive, a
project management tool for businesses.

%-----------------------------------------------------------------------------%
\subsection{Work Results}
%-----------------------------------------------------------------------------%

\subsubsection{Codebase Generator Tool}

In the first two weeks, a simple CRUD app was built with the Django backend
template and the Ionic Angular template using the codebase generator tool.
After identifying the issues while building the app, some fixes were proposed.
One of the issues were that some of the template files would not get copied
over when spawning a new project. The issue was fixed by modifying the
template's configuration files. Another crucial issue was that using NGINX to
serve Django static files caused some routing issues with \instansi's
infrastructure setup. This was fixed by replacing NGINX with a simpler
WhiteNoise setup, as shown by \autoref{fig:nginx}. The top side of the figure
shows the addition of the WhiteNoise middleware in the project settings
(\verb|cookiecutter/settings.py|), while the bottom side shows the removal of
the NGINX service in the Docker Compose configuration
(\verb|docker-compose.deploy.yml|).

\begin{figure}
	\centering
	\includegraphics[width=1.00\textwidth]
	{pics/nginx.png}
	\caption{
		An excerpt of the merge request that replaces NGINX with WhiteNoise.}
	\label{fig:nginx}
\end{figure}

\subsubsection{\instansi's Company Website}

The Success Stories page and other improvements to \instansi's website were
successfully implemented. Before implementing the page, the task was to create
a mock-up, as shown in \autoref{fig:mockup}. The page went through multiple
design iterations until it reached the final version shown in
\autoref{fig:successstories}. Other improvements, such as optimizations based
on Lighthouse reports, layout fixes, and form submission using JavaScript can
be seen in the merge requests shown in \autoref{fig:website}.

\begin{figure}
	\centering
	\includegraphics[width=0.75\textwidth]
	{pics/mockup.png}
	\caption{
		A mock-up of the Success Stories page on Figma.}
	\label{fig:mockup}
\end{figure}

\begin{figure}
	\centering
	\includegraphics[width=0.90\textwidth]
	{pics/successstories.png}
	\caption{
		The final Success Stories page implementation.}
	\label{fig:successstories}
\end{figure}

\begin{figure}
	\centering
	\includegraphics[width=0.90\textwidth]
	{pics/website.png}
	\caption{
		Other improvements to \instansi's website.}
	\label{fig:website}
\end{figure}

\subsubsection{Aiconix Slack Bot}

For the Aiconix Slack Bot project, the main issue was that the backend could
not determine the duration of certain media files uploaded by users. This issue
happened when the media files do not store the duration information at the top
level. Instead, they store it in the stream container. Some media files even do
not store it at all. Therefore, it could not be retrieved directly by ffprobe,
the tool used to gather information from multimedia streams. The problem was
solved by looking at the timestamp and duration of the last stream packet in
the media file (also using ffprobe). Other bug fixes and enhancements were also
implemented for the Aiconix Slack Bot project. In the backend, some of the
improvements were file corruption detection (merge request 138) and better
error handling (merge request 131) as shown in \autoref{fig:aiconix_backend}.
Some improvements to the frontend, such as translation fixes (merge request 89)
and service charge information (merge request 88), are shown in
\autoref{fig:aiconix_frontend}.

\begin{figure}
	\centering
	\includegraphics[width=0.90\textwidth]
	{pics/aiconix_backend.png}
	\caption{
		Improvements to the Aiconix Slack Bot backend.}
	\label{fig:aiconix_backend}
\end{figure}

\begin{figure}
	\centering
	\includegraphics[width=0.90\textwidth]
	{pics/aiconix_frontend.png}
	\caption{
		Improvements to the Aiconix Slack Bot frontend.}
	\label{fig:aiconix_frontend}
\end{figure}

\subsubsection{Taleegator}

The primary task for the Taleegator project was to set up CI/CD. The CI/CD
configuration consisted of linting, unit tests, building the Docker image, and
deployment to \instansi's private server. GitLab CI, Docker, and Docker Compose
were utilized to finish the task. \autoref{fig:taleegator} shows that the task
was done successfully as the deployment job was marked with a green check
badge after a coworker merged the configuration to the master branch and
triggered a pipeline.

\begin{figure}
	\centering
	\includegraphics[width=0.90\textwidth]
	{pics/taleegator.png}
	\caption{The successful pipeline for the master branch.}
	\label{fig:taleegator}
\end{figure}

\subsubsection{Augmented Science}

The Augmented Science project took up the last two weeks of the internship. For
this project, the primary task was to build a backend service. This service
stores image and video pairs that are uploaded by users from the web
application frontend. The service will then upload the image and metadata to
the Vuforia Web Service, a third-party service that provided image recognition
capabilities. The mobile application will track any recognized image and load
the related video using the link included in the metadata.
\autoref{fig:augmentedscience} shows some of the backend implementation work,
which included creating REST API views (merge request 9), adding Vuforia
integration (merge request 16), and implementing background tasks (merge
request 27). \autoref{fig:augmentedvideo1} shows the mobile app successfully
integrated with the Vuforia third-party API prior to recognizing the image
shown on a laptop screen. \autoref{fig:augmentedvideo2} shows the mobile app
successfully playing a video on top of the recognized image.

\begin{figure}
	\centering
	\includegraphics[width=0.90\textwidth]
	{pics/augmentedscience.png}
	\caption{
		The merge requests to develop the backend service for Augmented
		Science.}
	\label{fig:augmentedscience}
\end{figure}

\begin{figure}
	\centering
	\includegraphics[width=0.90\textwidth]
	{pics/augmentedvideo1.jpg}
	\caption{Screenshot of the Augmented Science app (before recognizing the image).}
	\label{fig:augmentedvideo1}
\end{figure}

\begin{figure}
	\centering
	\includegraphics[width=0.90\textwidth]
	{pics/augmentedvideo2.jpg}
	\caption{Screenshot of the Augmented Science app (after recognizing the image).}
	\label{fig:augmentedvideo2}
\end{figure}

%-----------------------------------------------------------------------------%
\subsection{Non-technical Aspects}
%-----------------------------------------------------------------------------%

There were also non-technical experiences gained from the internship,
especially in the communication aspect. The internship work hours had to be
adjusted to the 5-hour timezone difference with the supervisor and 4-hour
timezone difference with the other intern. In addition to that, professional
communication had to be done in English, both textually and verbally.

One important non-technical aspect learned was about self-discipline. With the
completely remote work setting, there was no direct supervision. Outside of the
core working hours (15.00-19.00 Jakarta time), the remaining time (2 hours) had
to be allocated independently. Therefore, the internship also required some
balancing between personal and professional life at home.

%-----------------------------------------------------------------------------%
\section{Analysis}
%-----------------------------------------------------------------------------%

This section provides an analysis of the internship work execution and its
relevance to the courses taken at the Faculty of Computer Science, Universitas
Indonesia (Fasilkom UI).

%-----------------------------------------------------------------------------%
\subsection{Internship Work Execution}
%-----------------------------------------------------------------------------%

This subsection talks about the comparison between the internship work
execution and terms of reference. It also talks about the obstacles encountered
throughout the internship and ways to overcome them, as well as an appraisal of
the place of internship.

\subsubsection{Comparison to the Internship Terms of Reference}

After the second week, the internship work execution was very different from
the work outlined in the internship terms of reference. This was caused by a
change of direction from \instansi's side. In the second week of the
internship, one of \instansi's developers offered to rebuild the codebase
generator tool from scratch and possibly turn it into an open source project.
Given how complex the existing template had become, \instansi\ also considered
how difficult the internship would be if the work was focused on the template.
It would require a deep understanding of the existing template and customer
projects the template was based on. Therefore, \instansi\ thought that the
internship would be a much better learning experience if the work were focused
on something else.

Instead of the codebase generator tool, \instansi\ offered to change the
internship work to be for the company's website and some of the customer
projects. The work done for the customer projects would be compensated on an
hourly rate. The offer was accepted, thus the rest of the internship went very
different from the outline presented in the terms of reference. The differences
between the work plan and the actual work done for each week can be seen in
\autoref{table:torcomparison}. In addition, the actual work days also included
Monday, while the terms of reference only stated Tuesday to Friday.

\begin{table}
	\centering
	\begin{tabular}{|P{3.65cm}|P{5cm}|P{5cm}|}
		\hline
		Time & Work Plan & Actual Work Done \\ \hline
		Week 1 \newline
		(14-17 July 2020) &
		Trying out the tool to build a simple project and taking notes of what
		works and what does not. &
		Trying out the tool to build a simple project and taking notes of what
		works and what does not. \\ \hline

		Week 2 \newline
		(21-24 July 2020) &
		Analyzing the issues and requirements of the tool and discussing the
		best approaches to fix and implement them. &
		Analyzing the issues and requirements of the tool and discussing the
		best approaches to fix and implement them. \\ \hline

		Week 3 \newline
		(28-31 July 2020) &
		Fixing the existing issues within the tool. &
		Working on \instansi's company website and the Aiconix Slack Bot
		project. \\ \hline

		Week 4 \newline
		(4-7 August 2020) &
		Backporting code for features (e.g. UI components,
		internationalization, etc.) of previous products to be incorporated
		into the tool. &
		Continued working on \instansi's company website and the Aiconix Slack
		Bot project, as well as fixing some issues in the codebase generator
		tool. \\ \hline

		Week 5 \newline
		(11-14 August 2020) &
		Writing tests and setting them up to run on a CI service. &
		Worked on the Aiconix Slack Bot, Taleegator, and Augmented Science
		projects. \\ \hline

		Week 6 \newline
		(18-21 August 2020) &
		Gathering feedback of the new implementation from the development team
		and improving the tool upon it. &
		Worked on the Aiconix Slack Bot project and finished the MVP for
		Augmented Science. \\ \hline
	\end{tabular}
	\caption{Initial work plan vs. its realization.}
	\label{table:torcomparison}
\end{table}

\subsubsection{Obstacles and Ways to Overcome Them}

In the beginning of the internship, it was difficult to adjust to the work
hours. Due to the existence of other activities in the morning, it was hard to
stay focused later in the day. This difficulty was overcome by taking more time
to rest in the morning and preserve the energy for the work hours.

There was also loss of context in the first few weeks of working on the
customer projects. This was mainly because the the projects had already started
a long time before the internship. Thus, it took some time to understand what
the projects were about. This was overcome by reading the projects' code and
documentation, as well as asking questions to other developers.

\subsubsection{Appraisal of the Place of Internship}

The experience of doing an internship at \instansi\ was very pleasant. The
supervisor was very accommodating to requests and suggestions. The other
employees at \instansi\ were also very supportive and helpful. The CEO really
cares about his employees and actively tries to make them comfortable working
at \instansi.

There were a few downsides to working at \instansi. One downside was the
considerable timezone difference, which may be off-putting for some people.
However, that might not be much of an issue as the core hours were still in a
reasonable timeframe in Indonesia. Another downside was the lack of direct
supervision and face-to-face communication due to the completely remote work
setting. On the other hand, considering the COVID-19 situation during the
internship, remote work was deemed to be beneficial.

\subsection{Relevance to Fasilkom UI Courses}

There are many courses at Fasilkom UI that were relevant to the internship.
Most of the courses are those related to software engineering. Some of the most
relevant courses are:

\begin{itemize}[wide, labelwidth=!, labelindent=0pt]
	\item \bo{Web Design and Programming}\\
	This course teaches web development using the Django web framework. While
	the course doesn't go through advanced topics such as the use of the Django
	REST Framework, it provides a strong basic in understanding how web
	development works. Aside from writing code, the knowledge of building
	mock-ups and configuring CI/CD learned from this course were also applied
	in the internship.
	\item \bo{Software Engineering}\\
	This course teaches software design principles and techniques in the
	software development process. The techniques learned from this course were
	applied while working on the projects at \instansi, such as requirements
	analysis, creating use case diagrams, class diagrams, etc. The agile
	software development learned in this course was also applied throughout the
	internship.
	\item \bo{Software Engineering Project}\\
	In this course, students work on a customer project from start to finish.
	Students are also expected to apply the best practices commonly found in
	the technology industry. The overall experience of working on a customer
	project in this course was very beneficial to the internship process.
\end{itemize}
