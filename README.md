# Improving Internal Projects and Developing Customer Products at Viper Development UG

| NPM  | Name                   | Year | Program | University |
| ---- | ---------------------- | -------- | ------- | ---------------- |
| 1706979455 | Sage Muhammad Abdullah | 2017 | Computer Science | Universitas Indonesia |

----------

## Abstract

To complete the Internship course, an internship was done at for six weeks,
starting from 14 July 2020 until 21 August 2020. The internship was for a
Software Developer role in several internal and client projects. The internship
tasks were to debug and develop new features in existing and new projects. The
internship was done from home with a team of software developers from countries
like Germany, Turkey, and India. During the internship, technologies such as
the Django REST Framework and Angular were utilized to build web-based
applications. Other than programming skills, the internship also gave the
opportunity to improve communication and collaboration skills in a remote-first
work environment with quite significant time zone differences.

## Notes

Written using the UI-style thesis latex template made by Andreas Febrian
(Fasilkom UI 2003), Erik Dominikus (Fasilkom UI 2007), Azhar Kurnia
(Fasilkom UI 2016), and Ichlasul Affan (Fasilkom UI 2016), with some
improvements and modifications.

## Download

Download the PDF from the latest
[pipeline](https://gitlab.com/laymonage/internship-report/-/pipelines).
